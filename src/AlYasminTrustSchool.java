import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class AlYasminTrustSchool {
    HashMap<Integer, ArrayList<String>> rollNumberAndName = new HashMap<>();

    AlYasminTrustSchool(Integer roll, String firstName, String lastName){
        ArrayList<String> fullName = new ArrayList<>();
        fullName.add(firstName);
        fullName.add(lastName);
        rollNumberAndName.put(roll, fullName);
    }

    public void addStudentInDatabase(Integer roll, String firstName, String lastName){
        if(this.rollNumberAndName.get(roll) == null){
            ArrayList<String> fullName = new ArrayList<>();
            fullName.add(firstName);
            fullName.add(lastName);
            rollNumberAndName.put(roll, fullName);
        }
        else{
            System.out.println("The roll number already exists in the database, try another");
        }
    }

    public void printDatabase(){
        for(Map.Entry<Integer, ArrayList<String>> element: rollNumberAndName.entrySet()){
            System.out.print(element.getKey() + ": ");
            System.out.println(element.getValue());
        }

    }

    public void removeStudentFromDatabase(Integer roll){
        if(this.rollNumberAndName.get(roll) != null){
            rollNumberAndName.remove(roll);
            System.out.println("Removed student from database");
        }
        else{
            System.out.println("The roll number doesn't exist in the database, try another");
        }
    }

    public void updateInformation(Integer roll){
        if(this.rollNumberAndName.get(roll) != null){
            Scanner scan = new Scanner(System.in);
            System.out.print("Enter the First name: ");
            String firstName = scan.next();
            System.out.print("Enter the Last name: ");
            String lastName = scan.next();
            ArrayList<String> fullName = new ArrayList<>();
            fullName.add(firstName);
            fullName.add(lastName);

            rollNumberAndName.put(roll, fullName);
            System.out.println("Updated database");

        }
        else{
            System.out.println("The roll number doesn't exist in the database, try another");
        }
    }

    public static void main(String[] args){
        AlYasminTrustSchool database = new AlYasminTrustSchool(1, "Isha", "Singh");

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter Y for admin, N for others: ");
        String adminOrNot = scan.nextLine();

        if(adminOrNot.equals("Y")){
            while(true){
                System.out.println("Press 1 to add student, Press 2 to view the database, Press 3 to remove student, Press 4 to update information,  Press 5 to exit");
                int inputFromUser  = scan.nextInt();

                if(inputFromUser == 1){
                    System.out.print("Enter the First name: ");
                    String firstName = scan.next();
                    System.out.print("Enter the Last name: ");
                    String lastName = scan.next();
                    System.out.print("Enter the Roll No: ");
                    Integer rollNo = scan.nextInt();

                    database.addStudentInDatabase(rollNo, firstName, lastName);
                }
                else if(inputFromUser == 2){
                    database.printDatabase();
                }
                else if(inputFromUser == 3){
                    System.out.print("Enter the Roll No: ");
                    Integer rollNo = scan.nextInt();
                    database.removeStudentFromDatabase(rollNo);
                }
                else if(inputFromUser == 4){
                    System.out.print("Enter the Roll No: ");
                    Integer rollNo = scan.nextInt();
                    database.updateInformation(rollNo);
                }
                else{
                    break;
                }
            }
        }
        else{
            while(true) {
                System.out.println("Press 1 view the database, Press 2 to exit");
                int inputFromUser = scan.nextInt();

                if (inputFromUser == 1) {
                    System.out.println("The database is: ");
                    database.printDatabase();
                } else {
                    break;
                }
            }
        }
    }

}
